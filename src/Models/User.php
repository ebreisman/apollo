<?php
namespace App\Models;

use RedBeanPHP\R as R;

class User extends Model
{
    private $username;
    private $password;
    private $role;

    public function __construct()
    {
        parent::__construct();
    }

    public function readByUsername(string $username)
    {
        $user = R::findOne('user', ' username = ? ', [$username]);
        if ($user) {
            $this->username = $user->username;
            $this->password = $user->password;
            $this->role     = $user->role;
        }
    }

    public function create(string $username, string $password, string $role)
    {
        $user = R::dispense('user');
        $user->username = $username;
        $user->password = password_hash($password, PASSWORD_DEFAULT);
        $user->role     = $role;

        $id = R::store($user);

        return $id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    /**
     * @return array
     */
    public function getRoles(): string
    {
        return $this->role;
    }

    /**
     * @param array $roles
     */
    public function setRole(string $roles)
    {
        $this->roles = $roles;
    }

    public function getPasswordHash()
    {
        return $this->password;
    }

    public function setPasswordHash(string $passwordHash)
    {
        $this->password = $passwordHash;
    }
}
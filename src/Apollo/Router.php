<?php

namespace App\Apollo;

class Router
{
    protected $controllerNameSpace;
    protected $defaultController;
    protected $filterNameSpace;

    public function __construct(string $controllerNameSpace)
    {
        $this->controllerNameSpace = $controllerNameSpace;
        $this->defaultController   = $controllerNameSpace . '\\HomeController';
    }

    /**
     * @return Route
     */
    public function route(): Route
    {
        $requestMethod  = $_SERVER['REQUEST_METHOD'];
        $path           = explode('/', $_SERVER['REQUEST_URI']);
        $controllerName = ucfirst($path[1]);
        $paramPath      = implode('/', array_slice($path, 2));

        $controller = $this->getController($controllerName);
        [$method, $args] = $this->getMethodAndArguments($requestMethod, $controller, $paramPath);

        return new Route($controller, $method, $args);
    }

    /**
     * Instantiate the controller object
     *
     * @param $controller
     * @param $request
     *
     * @return mixed
     */
    private function getController(string $controller): Controller
    {
        if (empty($controller)) {
            return new $this->defaultController();
        }

        $class = $this->controllerNameSpace . '\\' . $controller . 'Controller';
        if (!class_exists($class)) {
            http_response_code(404);
            die();
        }
        return new $class();
    }

    /**
     * Instantiate and return the reflected method and it parameters
     *
     * @param $requestMethod
     * @param $controller
     * @param $methodPath
     *
     * @return array
     */
    private function getMethodAndArguments(
        string $requestMethod,
        Controller $controller,
        string $methodPath
    ): array
    {
        $paths  = $controller->requestMap();
        $method = null;

        foreach ($paths as $regex => $methods) {
            $regex = $this->normalizeRegex($regex);
            if (preg_match('/' . $regex . '/', $methodPath, $matches)) {
                foreach ($methods as $requestType => $methodName) {
                    if ($requestType === $requestMethod) {
                        $method = new \ReflectionMethod($controller, $methodName);
                        return [$method, $matches];
                        break 2;
                    }
                }
            }
        }

        http_response_code(404);
        die();
    }

    /**
     * Convert from the simple regex expression in the controller to php regex expression
     *
     * @param $regex
     *
     * @return mixed
     */
    private function normalizeRegex(String $regex): string
    {
        $regex = preg_replace('/\//', '', $regex, 1);
        $regex = str_replace('/', '\/', $regex);
        $regex = str_replace('<:', '(?<', $regex);
        $regex = str_replace('>', '>\w+)', $regex);
        return $regex;
    }
}

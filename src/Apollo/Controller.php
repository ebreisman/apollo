<?php
namespace App\Apollo;

abstract class Controller
{
    protected $route;

    public function __construct()
    {
    }

    public function setRoute(Route $route): void
    {
        $this->route = $route;
    }

    public function getRoute(): Route
    {
        return $this->route;
    }

}
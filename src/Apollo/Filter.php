<?php
namespace App\Apollo;

abstract class Filter implements Runnable {

    protected $route;
    protected $next;

    public function __construct(Route $route, ?Filter $next)
    {
        $this->route = $route;
        $this->next  = $next;
    }

    public function setNext($next): void
    {
        $this->next = $next;
    }

    abstract function run();
}
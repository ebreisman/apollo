<?php
namespace App\Apollo;

class Application {

    private static $config;

    public function __construct()
    {
        self::$config = parse_ini_file(__DIR__ . '/../Config/app.ini', true);
    }

    public static function getConfig()
    {
        return self::$config;
    }

    public function start()
    {
        ob_start();
        $router   = new Router(self::$config['controllers']['namespace']);
        $route    = $router->route();
        $response = $route->execute();
        $response->send();
        ob_end_flush();
    }
}
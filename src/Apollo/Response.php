<?php
/**
 * Created by PhpStorm.
 * User: evanr
 * Date: 6/26/2017
 * Time: 8:26 PM
 */

namespace App\Apollo;


class Response
{
    private $data;
    private $headers;
    private $statusCode;

    public function __construct($statusCode = 200, $data = '', $headers = [])
    {
        $this->data       = $data;
        $this->headers    = $headers;
        $this->statusCode = $statusCode;
    }

    public function send()
    {
        header($this->statusCode);

        foreach ($this->headers as $header) {
            header($header);
        }

        echo $this->data;
    }
}
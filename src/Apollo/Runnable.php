<?php
/**
 * Created by PhpStorm.
 * User: evanr
 * Date: 6/25/2017
 * Time: 11:47 AM
 */

namespace App\Apollo;


interface Runnable
{
    public function run();
}
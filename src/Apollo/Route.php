<?php

namespace App\Apollo;

class Route implements Runnable
{
    protected $controller;
    protected $method;
    protected $args = [];
    protected $filterNamespace = '\\App\\Filters';
    protected $argsToParamsMap;

    public function __construct(Controller $controller, \ReflectionMethod $method, array $args = [])
    {
        $this->controller      = $controller;
        $this->method          = $method;
        $this->args            = $args;
        $this->argsToParamsMap = $this->mapArgsToParams();
    }

    public function get($variable)
    {
        return filter_input(INPUT_GET, $variable, FILTER_SANITIZE_STRING);
    }

    public function post($variable)
    {
        return filter_input(INPUT_POST, $variable, FILTER_SANITIZE_STRING);
    }

    public function cookie($variable)
    {
        return filter_input(INPUT_COOKIE, $variable, FILTER_SANITIZE_STRING);
    }


    public function setFilterNameSpace(string $namespace)
    {
        $this->filterNamespace = $namespace;
    }

    /**
     * Map the arguments to the methods parameters
     *
     * @return array
     */
    private function mapArgsToParams(): array
    {
        $argsToParams = [];
        foreach ($this->parameters() as $param) {
            $value = $this->args[$param] ?? null;
            if ($value !== null) {
                $argsToParams[$param] = $value;
            }
        }

        return $argsToParams;
    }

    /**
     * Flatten array of parameters
     *
     * @return array
     */
    public function parameters(): array
    {
        $parameters = $this->method->getParameters();
        $result     = [];
        foreach ($parameters as $parameter) {
            $result[] = $parameter->getName();
        }

        return $result;
    }

    /**
     * Run the call chain
     */
    public function execute()
    {
        $filters = $this->createFilters();

        // Run the method if there are no filters
        if (empty($filters)) {
            $response = $this->run();
            return $response;
        }

        // If there are filters, link them and the method together
        foreach ($filters as $index => $filter) {
            if (array_key_exists($index + 1, $filters)) {
                $filters[$index]->setNext($filters[$index + 1]);
            } else {
                $filters[$index]->setNext($this);
            }
        }

        // Run the first filter, which starts the chain of method calls
        return $filters[0]->run();
    }

    /**
     * Instantiate the filters associated with this method
     *
     * @return Filter[]
     */
    protected function createFilters(): array
    {
        $filters     = [];
        $filterNames = $this->annotations('Filters');

        foreach ($filterNames as $filterName) {
            $filterFullName = $this->filterNamespace . '\\' . $filterName;
            if (!class_exists($filterFullName)) {
                http_response_code(500);
                die();
            }

            $filter    = new $filterFullName($this, null);
            $filters[] = $filter;
        }

        return $filters;
    }

    /**
     * Retrieve the annotations on this class and method
     *
     * @param $type of annotation (e.g. Filters, Roles, etc).
     *
     * @return array
     */
    public function annotations(string $type): array
    {
        $pattern = "/@$type\((.+)\)/";

        preg_match($pattern, $this->method->getDeclaringClass()->getDocComment(), $classAnnotations);
        if (!empty($classAnnotations)) {
            $classAnnotations = explode(',', $classAnnotations[1]);
        }

        preg_match($pattern, $this->method->getDocComment(), $methodAnnotations);
        if (!empty($methodAnnotations)) {
            $methodAnnotations = explode(',', $methodAnnotations[1]);
        }

        $annotations = array_unique(array_merge($classAnnotations, $methodAnnotations));
        $annotations = array_map('trim', $annotations);

        return $annotations;
    }

    /**
     * Run only the routed method
     */
    public function run()
    {
        $this->controller->setRoute($this);
        return $this->method->invokeArgs($this->controller, $this->argsToParamsMap);
    }
}
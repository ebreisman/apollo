<?php
/**
 * Created by PhpStorm.
 * User: evanr
 * Date: 6/10/2017
 * Time: 8:58 PM
 */

use App\Apollo\Router;

class RouterTest extends \PHPUnit\Framework\TestCase
{

    public function testRouter()
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $_SERVER['REQUEST_URI']    = '/home/evan/reisman';

        $router = new Router('\\App\\Apollo\\Tests\\Controllers');

        $route = $router->route();
        $route->setFilterNameSpace('\\App\\Apollo\\Tests\\Filters');
        $response = $route->execute();

        $this->assertNotNull($route);
    }
}

<?php

namespace App\Apollo\Tests\Controllers;

use App\Apollo\Controller;
/**
 * Created by PhpStorm.
 * User: evanr
 * Date: 6/4/2017
 * Time: 12:10 PM
 * @Route(home)
 * @Filters(AuthFilter)
 */
class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function requestMap(): array
    {
        return [
            "/" => [
                'GET'    => 'getRoot',
                'POST'   => 'postRoot',
            ],
            "/<:first>/<:last>" => [
                'GET' => 'index'
            ],
            "/<:account>/person"   => [
                'GET'  => 'createNewPerson'
            ],
            "/info" => [
                'GET' => 'info'
            ],
            "/about" => [
                'GET' => 'about'
            ]
        ];
    }

    public function getRoot()
    {
        return 'getRoot';
    }

    public function postRoot()
    {
        return 'postRoot';
    }

    /**
     * @Roles(USER)
     * @Filters(AuthFilter,LogFilter)
     *
     * @param $account
     */
    public function createNewPerson($account): void
    {
        echo 'new person ' . $account;
    }

    /**
     * @Roles(ADMIN,USER)
     * @Filters(AuthFilter,LogFilter)
     */
    public function index($first, $last) {
        $template = $this->twig->load('home/index.html');
        echo $template->render(['first' => $first, 'last' => $last]);
    }

    public function about()
    {
        echo 'About';
    }

    public function info()
    {
        phpinfo();
    }
}
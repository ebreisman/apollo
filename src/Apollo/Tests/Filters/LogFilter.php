<?php
namespace App\Apollo\Tests\Filters;

use App\Apollo\Filter;

/**
 * Created by PhpStorm.
 * User: evanr
 * Date: 6/18/2017
 * Time: 4:56 PM
 */
class LogFilter extends Filter {

    public function run()
    {
        echo 'In log filter ';
        $this->next->run();
        echo 'Back in log filter ';
    }
}
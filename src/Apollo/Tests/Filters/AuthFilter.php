<?php
namespace App\Apollo\Tests\Filters;

use App\Apollo\Filter;


/**
 * Created by PhpStorm.
 * User: evanr
 * Date: 6/11/2017
 * Time: 9:35 PM
 */
class AuthFilter extends Filter {

    public function run()
    {
        echo 'In auth filter ';
        $roles = $this->route->annotations('Roles');
        $this->next->run();
        echo 'Back in auth filter ';
    }
}
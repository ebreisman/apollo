<?php
/**
 * Created by PhpStorm.
 * User: evanr
 * Date: 6/26/2017
 * Time: 8:17 PM
 */

namespace App\Tests\Controllers;

use App\Controllers\LoginController;
use PHPUnit\Framework\TestCase;

class LoginControllerTest extends TestCase
{
    public function testGetLoginPage()
    {
        $login    = new LoginController();
        $response = $login->loginPage();

        $this->assertNotEmpty($response);
    }

    public function testPostLogin()
    {
        $login = new LoginController();
        $response = $login->login();

    }
}

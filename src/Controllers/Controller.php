<?php
namespace App\Controllers;

use Twig_Loader_Filesystem;
use Twig_Environment;

class Controller extends \App\Apollo\Controller
{
    protected $twig;

    public function __construct()
    {
        parent::__construct();

        $loader = new Twig_Loader_Filesystem(__DIR__ . '/../Views');

        $this->twig = new Twig_Environment($loader, [
            'debug'       => true,
            'cache'       => false,
            'auto_reload' => true
        ]);
    }
}
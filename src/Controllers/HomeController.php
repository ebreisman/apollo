<?php namespace App\Controllers;

use App\Controllers;
use App\Apollo\Response;

/**
 * @Filters(AuthenticationFilter, AuthorizationFilter)
 * @Roles(ADMIN, USER)
 * Class HomeController
 * @package App\Controllers
 */
class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function requestMap()
    {
        return [
            "/info" => [
                'GET' => 'info'
            ],
            "/" => [
                'GET' => 'index'
            ],
        ];
    }

    public function index() {
        $username = $_SESSION['username'];
        $template = $this->twig->load('home/index.html');
        $result   = $template->render(['username' => $username]);
        $response = new Response(200, $result);
        return $response;
    }

    public function info()
    {
        phpinfo();
    }
}
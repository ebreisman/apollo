<?php
namespace App\Controllers;

use App\Apollo\Response;
use App\Services\UserService;

class LoginController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function requestMap()
    {
        return [
            '/logout'   => [
                'GET' => 'logout'
            ],
            '/register' => [
                'GET'  => 'registerPage',
                'POST' => 'register'
            ],
            '/'         => [
                'GET'  => 'loginPage',
                'POST' => 'login'
            ],
        ];
    }

    public function logout()
    {
        session_destroy();
        return new Response(200, '', ['Location: http://www.apollolaw.com/login']);
    }

    public function loginPage()
    {
        $template = $this->twig->load('login/index.html');
        $result   = $template->render();
        $response = new Response(200, $result);
        return $response;
    }

    public function login()
    {
        $username = $this->route->post('username');
        $password = $this->route->post('password');

        $userService     = new UserService();
        $isAuthenticated = $userService->authenticateUser($username, $password);
        if ($isAuthenticated) {
            $response = new Response(302, '', ['Location: http://www.apollolaw.com/home']);
            return $response;
        } else {
            $template = $this->twig->load('login/index.html');
            $result = $template->render(['result' => 'nl']);
            $response = new Response(401, $result, []);
            return $response;
        }
    }

    public function registerPage()
    {
        $template = $this->twig->load('login/register.html');
        return $template->render();
    }

    public function register()
    {
        $userService = new UserService();
        $userService->registerUser($this->route->post('username'), $this->route->post('password'), 'ADMIN');
    }
}
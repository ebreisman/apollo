<?php
namespace App\Services;

use App\Apollo\Service;
use App\Models\User;

class UserService extends Service
{
    public function authenticateUser(string $username, string $password)
    {
        $user = new User();
        $user->readByUsername($username);
        if (password_verify($password, $user->getPasswordHash())) {
            $_SESSION['username'] = $user->getUsername();
            $_SESSION['role']     = $user->getRoles();
            return true;
        }

        return false;
    }

    public function registerUser(string $username, string $password, $role)
    {
        $user = new User();

        $id = $user->create($username, $password, $role);

        return $id;
    }

}
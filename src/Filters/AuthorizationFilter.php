<?php

namespace App\Filters;

use App\Apollo\Filter;

class AuthorizationFilter extends Filter
{
    private $level = [
        'GUEST'        => 10,
        'USER'         => 20,
        'ADMIN'        => 30,
        'OWNER'        => 40,
        'SYSTEM_USER'  => 50,
        'SYSTEM_ADMIN' => 60,
        'SYSTEM_OWNER' => 100
    ];

    public function run()
    {
        $methodRoles = $this->route->annotations('Roles');
        $userRole    = $_SESSION['role'];
        $authorized  = false;
        foreach ($methodRoles as $methodRole) {
            if ($this->level[$userRole] >= $this->level[$methodRole]) {
                $authorized = true;
                break;
            }
        }

        if (!$authorized) {
            http_response_code(401);
            die();
        }

        return $this->next->run();
    }
}
<?php
namespace App\Filters;

use App\Apollo\Filter;
use App\Apollo\Response;

class AuthenticationFilter extends Filter {

    public function run()
    {
        if (!isset($_SESSION['username'])) {
            session_destroy();
            return new Response(401, '', ['Location: http://www.apollolaw.com/login']);
//            die();
        };

        return $this->next->run();
    }
}
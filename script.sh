#!/bin/bash

sudo apt-get -y update

sudo apt-get -y install apache2

sudo apt-get -y install python-software-properties
sudo apt-add-repository -y ppa:ondrej/php
sudo apt-get -y update

sudo apt-get -y install php7.1 libapache2-mod-php7.1 php7.1-fpm php7.1-cli php7.1-common php7.1-mbstring php7.1-gd php7.1-intl php7.1-xml php7.1-mysql php7.1-mcrypt php7.1-zip php7.1-xdebug

sudo apt-get -y install software-properties-common
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
sudo apt-add-repository -y 'deb [arch=amd64,i386] http://ftp.osuosl.org/pub/mariadb/repo/10.2 zesty main'
sudo apt-get -y update
sudo apt-get -y install mariadb-server
sudo mysql_install_db --user=mysql

curl -s "https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh" | sudo bash
sudo apt-get install php7.1-phalcon
